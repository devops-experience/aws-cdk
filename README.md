# AWS CDK

## Apresentação
Download da apresentação:

[AWS CDK - DevOps Heroes](./AWS%20CDK%20-%20DevOps%20Heroes.pdf)

## Topologia

![](topology.png)

## Requisitos

É necessário ter uma conta da AWS, com acesso programático (geralmente Access Key e Secret Key) configurado.  
*ATENÇÃO:* A maioria dos recursos utilizados aqui estão dentro do _free-tier_ da AWS. Entretanto há recursos que podem gerar custos, como o envio de SMS.  

Para fazer deploy da stack em Cloudformation é necessário ter o [AWS CLI](https://aws.amazon.com/cli/) instalado e configurado.  
Para deploy da stack em CDK é necessário o Node.js (recomendada versão 16.x) instalado, além do AWS CLI instalado e configurado.
Todas as instruções de instalação e configuração podem ser encontradas [aqui](https://cdkworkshop.com/15-prerequisites.html).

## Deploy stack Cloudformation
Para fazer o deploy da stack em Cloudformation deve-se executar o seguinte comando.
- A opção "CAPABILITY_NAMED_IAM" é necessária sempre que são executadas ações no IAM da AWS através do Cloudformation.

```
aws cloudformation deploy --capabilities CAPABILITY_NAMED_IAM --template-file cloudformation/pipeline.yaml --stack-name cf-stack-sms
```

## Deploy stack AWS CDK
Comandos uteis ao trabalhar com AWS CDK:

```
# entrar no diretório onde está o código
cd aws-cdk

# Fazer o build do projeto
npm run build

# Executa os testes e criar snapshots
npm run test:update

# Apenas executar os testes
npm run test

# Sintetizar os arquivos Typescript em templates Cloudformation
cdk synth

# Exibir as diferenças de infraestrutura
cdk diff

# Fazer deploy da stack
cdk deploy

```
Dica: você pode encontrar todos os "comandos" (scripts) que podem ser executados com o npm no bloco de scripts do [package.json](aws-cdk/package.json)

## Invocar a função lambda via AWS CLI

Para invocar uma função lambda através da AWS CLI podemos executar o seguinte comando:

Dica: quando executamos uma função lambda, por padrão é necessário informar um arquivo para salvar o retorno da execução. Neste caso, como não temos interesse em salvar em arquivo, informamos o caminho /dev/null.

```
aws lambda invoke --function-name arn:aws:lambda:${REGION}:${ACCOUNT_ID}:function:<FUNCTION_NAME> /dev/null
```

## Cleanup
Não se esqueça de deletar as stacks criadas na console do Cloudformation afim de evitar custos surpresa.