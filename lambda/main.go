package main

import (
	"context"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
)

// HandleRequest is the entrypoint for the Lambda function
func HandleRequest(ctx context.Context) error {
	// Create a new AWS session
	sess, err := session.NewSession()
	if err != nil {
		return fmt.Errorf("error creating AWS session: %s", err)
	}

	// Create a new SNS client using the session
	svc := sns.New(sess)

	// Publish the message to the SNS topic
	_, err = svc.Publish(&sns.PublishInput{
		Message:  aws.String("Hello, DevOps Hero!"),
		TopicArn: aws.String(os.Getenv("SNS_TOPIC_ARN")),
	})

	if err != nil {
		return fmt.Errorf("error publishing message to SNS topic: %s", err)
	}

	fmt.Println("Successfully published message to SNS topic")

	return nil
}

func main() {
	lambda.Start(HandleRequest)
}
