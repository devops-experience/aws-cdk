import * as fs from 'fs';
import * as yaml from 'js-yaml';
import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as s3 from 'aws-cdk-lib/aws-s3';
import * as sns from 'aws-cdk-lib/aws-sns';
import { StackProps } from 'aws-cdk-lib';
import { LambdaAppStage } from './pipeline-stage';

export interface PipelineStackProps extends StackProps {
  serviceName: string;
}

export class PipelineStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props: PipelineStackProps) {
    super(scope, id, props);


    const cdkS3Bucket = new s3.Bucket(this, 'CdkStackBucket', {
      bucketName: "cdk-lambda-s3-bucket",
      versioned: true, 
    })

    const inputTriggerFile = cdk.pipelines.CodePipelineSource.s3(cdkS3Bucket, 'lambda-source.zip', {trigger: cdk.aws_codepipeline_actions.S3Trigger.EVENTS });


    const snsTopic = new sns.Topic(this, 'snsTopic', {
      displayName: "DevOpsExperience",
      topicName: "devops-heroes-cdk",
    })

    const lambdaBuildStep = new cdk.pipelines.CodeBuildStep('lambdaBuildStep', {
      input: inputTriggerFile,
      projectName: `${props.serviceName}-build`,
      primaryOutputDirectory: 'aws-cdk/cdk.out',
      commands: [],
      partialBuildSpec: cdk.aws_codebuild.BuildSpec.fromObject(yaml.load(fs.readFileSync('lib/buildspec.yaml', {encoding: 'utf-8'})) as Object),
      env: {
        S3_BUCKET_NAME: cdkS3Bucket.bucketName,
      },
      buildEnvironment: {
        buildImage: cdk.aws_codebuild.LinuxBuildImage.AMAZON_LINUX_2_4,
        computeType: cdk.aws_codebuild.ComputeType.SMALL,
        privileged: false,
      },
    });

    const lambdaPipeline = new cdk.pipelines.CodePipeline(this, 'lambdaPipeline', {
      synth: lambdaBuildStep,
      selfMutation: true,
    })

    lambdaPipeline.addStage(new LambdaAppStage(this, 'lambdaStack', {
      serviceName: props.serviceName,
      s3Bucket: cdkS3Bucket,
      snsTopicArn: snsTopic.topicArn,
    }), {
      pre: [lambdaBuildStep]
  });

    lambdaPipeline.buildPipeline();

    cdkS3Bucket.grantReadWrite(lambdaBuildStep);

  }
}
